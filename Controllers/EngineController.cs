﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entities;
using Contracts;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace RuleEngine.Controllers
{

    public class EngineController : Controller
    {
        private IOperation _operation;
        private IThreshold _threshold;

        public EngineController(IOperation operations,IThreshold threshold)
        {
            _operation = operations;
            _threshold = threshold;
        }
        public IActionResult Index()
        {
            return View(EngineRun());
        }
        public List<EngineResult> EngineRun()
        {
            DataProvider data = new DataProvider();

            var operationsResults = _operation.CalculateOperations(data.GetOperations());
            return _threshold.CalculateThreshold(operationsResults);

        }

 
    }
}